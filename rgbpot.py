import pigpio
import time
import busio
import adafruit_ads1x15.ads1115 as ADC
from adafruit_ads1x15.analog_in import AnalogIn

def rgb(p,dValeurs):
    for k in dValeurs.keys():
        cycle = 255 - dValeurs[k]
        p.set_PWM_dutycycle(k,cycle)

SCL = 3 # Num GPIO de broche SCL
SDA = 2 # Num GPIO de broche SDA
R,G,B = 17,27,22 #Broches GPIO pour LED
GAIN = 1
MAX = 26300 # La valeur maximale du potentiometre

# Intervalles
QUART=MAX*0.25
MOITIE=MAX*0.5
TQUARTS=MAX*0.75

# Init module ADC
i2cBus = busio.I2C(SCL, SDA)
adc = ADC.ADS1115(i2cBus, GAIN)

# Init GPIO
pi = pigpio.pi()
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

d ={}
a0 = 0

try:
    while True:
        a0 = AnalogIn(adc, 0)
        if a0 < QUART:
            d[R]=255
            d[G]=int(a0/QUART*255)
            d[B]=0
        elif QUART <= a0 < MOITIE:
            d[R]=int(255-((a0-QUART)/QUART*255))
            d[G]=255
            d[B]=0
        elif MOITIE <= a0 < TQUARTS: 
            d[R]=0
            d[G]=255
            d[B]=int((a0-MOITIE)/QUART*255)
        elif TQUARTS <= a0 < MAX:
            d[R]=0
            d[G]=int(255-((a0-TQUARTS)/QUART*255))
            d[B]=255

        rgb(pi,d)

except KeyboardInterrupt:
    pi.write(R,1)
    pi.write(G,1)
    pi.write(B,1)