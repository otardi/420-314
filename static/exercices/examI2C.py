import board
import busio
from time import sleep
from adafruit_ht16k33 import matrix
import adafruit_bus_device.i2c_device as i2c_device

i2c = busio.I2C(board.SCL, board.SDA)
initialisation = matrix.Matrix8x8(i2c)
module = i2c_device.I2CDevice(i2c, 0x70)

# Allume ou eteint toutes les LED selon le boolean passé
def remplir(mod,on=True):
    octet = 255 if on else 0
    for i in range(0,16,2): # 0,2,4,6,8...
        mod.write(bytes([i]))
        mod.write(bytes([i,octet]))

# Allume 1 LED 
def allumer(mod,rg,col):
    adr = rg << 1 # Trouver l'adresse de la rangée
    octet = 1 << col # Trouver le bit de la colonne

    # Voir ce qui est déjà allumé dans la rangée
    donnees = bytearray(1) # Variable qui va contenir les données
    mod.write(bytes([adr])) # Dire quelle adresse on veut utiliser
    module.readinto(donnees) # Lire et mettre le contenu dans la variable
    
    # Combiner 2 octets (celui qu'on veut allumer 
    # + celui qui l'est deja)
    octet = donnees[0] | octet
    mod.write(bytes([adr])) # Dire quelle adresse on veut utiliser
    module.write(bytes([adr,octet])) # Ecrire l'octet à l'adresse

# Allume un carré de 4x4 LED
def carre(mod):
    rangees = [0,0,60,60,60,60,0,0]
    i=0
    for oct in rangees:
        adr = i << 1
        mod.write(bytes([adr]))
        mod.write(bytes([adr,oct]))
        i+=1

# Inverse toutes les LED:
# Toutes celles qui sont allumées s'éteignent
# Toutes celles qui sont éteintes s'allument
# La fonction doit utiliser un opérateur bitwise
# Supposons qu'on veut inverser 110011 pour avoir 001100:
# 110011
# ?????? <- Trouvez cette valeur
# ------ <- Trouvez l'opérateur
# 001100
def flip(mod):
    i = 0
    while i < 8: # Pour chaque rangée
        # Trouver son adresse
        # Lire la valeur à cette adresse
        # Inverser les bits lus
        # Écrire les bits inversés à l'adresse
        i+=1

remplir(module,False) # Tout éteindre
carre(module) # Afficher le carré
sleep(1)
flip(module) # Inverser les LED


