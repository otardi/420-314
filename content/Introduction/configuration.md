---
title: "Configuration"
date: 2023-08-20T17:44:26-04:00
draft: false
weight: 12
---

## Accès au *Raspberry Pi*
L'accès aux objets connectés se fait rarement en y branchant un clavier, un écran et une souris: généralement on y accède à distance, en se connectant via un réseau informatique. 

Dans cette section, nous allons mettre en place ce qu'il faut pour que le *Raspberry Pi* puisse accepter les connexions distantes.

> Pour cette section, votre Pi doit être connecté à un écran, un clavier et une souris et le système d'exploitation *Raspberry Pi OS* doit être installé.

#### Changement du nom d'hôte
Afin qu'il puisse être reconnu sur le réseau sans qu'on ait à utiliser son adresse IP, on doit changer le nom d'hôte du Pi. Par défaut, ce nom est "raspberrypi". Choisissez un nom ayant de bonnes chances d'être unique sur le réseau, puis écrivez-le dans le fichier `/etc/hostname`:

![nanohostname](/420-314/images/nanohostname.png?width=400px)

#### Connexion au réseau
Afin que votre PC et le Pi soient sur le même réseau, connectez d'abord le Pi sur le réseau du département, puis (après quelques secondes) vérifiez qu'il a bien obtenu une adresse avec la commande `ip address`. L'adresse devrait avoir une valeur entre `10.10.0.1` et `10.10.31.254`:

![cnxlan](/420-314/images/cnxlan.png?width=400px)

![ip_a](/420-314/images/ip_a.png?width=400px)

#### Installation des services
##### Serveur SSH
Pour rendre possible l'accès au Pi par SSH (avec *putty* par exemple) il faut y installer le serveur SSH et s'assurer qu'il est lancé à chaque démarrage. Les commandes sont les suivantes:
```bash
sudo systemctl start ssh
sudo systemctl enable ssh
```
##### Serveur VNC
Pour rendre possible l'accès au Pi par VNC (ce qui permet d'utiliser le bureau à distance), il faut installer le serveur VNC comme suit:
```bash
sudo apt install realvnc-vnc-server realvnc-vnc-viewer
```
Ensuite on doit activer l'interface VNC. 
+ Lancez la commande `sudo raspi-config`
+ Choisissez `Interface options` -> `VNC` -> `Yes`

#### Tests
Redémarrez le RaspberryPi.

Ensuite, connectez le PC sur le même réseau que le Pi puis vérifiez s'il a bien obtenu une adresse avec la commande `ipconfig`. Ici aussi L'adresse devrait avoir une valeur entre `10.10.0.1` et `10.10.31.254`.

##### SSH
Puisque vous avez changé son nom d'hôte, votre *RaspberryPi* devrait être joignable en utilisant ce nom suivi du suffixe `.local`. Donc si par exemple mon Pi a `prof` comme nom d'hôte, je dois utiliser `prof.local` pour m'y connecter.

Ouvrez une session sur votre Pi avec *putty*:
![puttycnx](/420-314/images/puttycnx.png?width=400px)

Si le service est correctement configuré, vous devriez avoir accès à la ligne de commande sur le Pi.

##### VNC
Vous devez utiliser le programme *VNC viewer* pour accéder au bureau à distance. 

![vncview](/420-314/images/vncview.png?width=400px)

Au démarrage du programme, utilisez le nom d'hôte de votre Pi pour y établir une connexion:

![vncconf](/420-314/images/vncconf.png?width=400px)

Vous devrez confirmer que vous souhaitez bien vous connecter en cliquant sur `Continuer`. Ensuite, il suffit d'entrer votre identifiant et mot de passe du Pi.

Lorsque tous les tests sont concluants, vous pourrez accéder au Pi simplement en connectant votre PC et le Pi sur le même réseau, en utilisant *putty* ou *VNC viewer*, sans avoir besoin de brancher le clavier, la souris ou l'écran.

## Environnement de développement
Pour permettre aux programmes de communiquer avec le RaspberryPi, il faut utiliser des librairies spécifiques (*pigpio* en python et *WiringPi* en C). Ces librairies ne sont toutefois pas installées dans le système d'exploitation: c'est ce que nous allons faire ici.

La première commande vise à mettre à jour tous les logiciels sur notre Pi afin d'éviter des problèmes de compatibilité:
```bash
sudo apt update 
sudo apt upgrade
```

#### piGPIO
Pour installer la librairie permettant à python d'utiliser les broches GPIO du Pi, la commande est celle-ci:
```bash
sudo apt install pigpio
```
Une fois installée, pour pouvoir communiquer avec le GPIO depuis un programme, le service correspondant doit être actif. Les deux commandes suivantes permettent de le démarrer et de s'assurer qu'il démarrera par la suite automatiquement chaque fois qu'on allume le Pi:
```bash
sudo systemctl start pigpiod
sudo systemctl enable pigpiod
```

#### WiringPi
Pour installer la librairie permettant au langage C d'utiliser les broches GPIO, il faut télécharger le paquet logiciel avec wget et l'installer avec dpkg. Les commmandes sont les suivantes:
```bash
wget https://project-downloads.drogon.net/wiringpi-latest.deb
sudo dpkg -i wiringpi-latest.deb
```
Ensuite, si l'installation s'est bien pasée, la commande `gpio -v` devrait afficher les informations de version de la librairie.


#### Développement en python
Il y a deux façons de développer vos programmes avec python sur le Pi. 

La première consiste à utiliser un éditeur de texte ordinaire à partir de la ligne de commande. Si vous êtes connecté sur votre Pi à l'aide de *putty*, vous devrez utiliser cette méthode.

Utilisez *nano* pour écrire votre programme:

![hellopy](/420-314/images/hellopy.png?width=400px)

Lancez ensuite le programme en appelant l'interpréteur comme suit:
```bash
pi@prof:~ $ python hello.py
hello
```

La deuxième consiste à utiliser l'IDE *Thonny*:
![thonny](/420-314/images/thonny.png?width=400px)

Celui-ci dispose des fonctionnalités essentielles d'un IDE de base: débogueur, points d'arrêts et une console au bas de l'interface. Pour exécuter un programme, cliquez sur *Run*:

![thonnyexec](/420-314/images/thonnyexec.png?width=400px)

#### Développement en C
Le logiciel *Geany* est un IDE qui permet de développer puis compiler des programmes en C mais pour l'instant, nous écrirons les programmes dans des fichiers texte qui seront ensuite compilés "à la main". Vous pouvez ici aussi utiliser un éditeur de texte sur la ligne de commande (par exemple *nano*), ou encore l'éditeur de texte *Mousepad* (`Menu -> Accessories -> Text editor`) dans l'environnement de bureau:

![helloc](/420-314/images/helloc.png?width=400px)

Ensuite, il faut compiler ce programme pour en faire un fichier exécutable avec la commande `gcc`. Lorsque le programme est compilé, on peut l'exécuter en l'appelant par son nom:
```bash
pi@prof:~ $ gcc hello.c -o hello
pi@prof:~ $ ./hello
Hello
```


