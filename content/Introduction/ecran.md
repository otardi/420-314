---
title: "Nano-PC"
date: 2023-09-02T17:32:58-04:00
draft: false
weight: 14
---

Dans cette section nous verrons comment relier l'écran tactile au *RaspberryPi* afin de pouvoir l'utiliser comme un PC ou une tablette.


{{% notice info "Remarque" %}}
+ Le Raspberry Pi doit avoir le système d'exploitation *RaspberryPi OS* installé sur sa carte SD
+ Assurez-vous d'avoir les câbles rouge et noir et le ruban blanc fournis avec l'écran.
{{% /notice %}}

## Assemblage

#### Raccordement électrique
Branchez respectivement les fils rouge et noir sur les broches 5v et GND de l'écran et vissez le Raspberry Pi sur l'écran à l'aide des 4 vis argentées:

![connectscreen](/420-314/images/connectscreen.png?width=400px)

Raccordez les fils rouge et noir sur les broches 4 et 6 du Pi:

![connscreen](/420-314/images/connscreen.png?width=400px)


Assurez-vous que les fermoirs noirs sont bien tirés sur les ports graphiques de l'écran et du Pi
![connnappe1](/420-314/images/connnappe1.png?width=400px)

Insérez le ruban dans les ports graphiques et resserrez les fermoirs:
![connnappe](/420-314/images/connnappe.png?width=400px)

Insérez l'écran dans le boîtier de plastique noir. Assurez-vous que les trous des vis sont bien alignés puis vissez le boîtier à l'écran à l'aide des 4 vis noires fournies.
![boitier](/420-314/images/boitier.png?width=400px)

Vous pouvez ensuite démarrer le Pi.


## Clavier tactile
Si vous souhaitez utiliser un clavier tactile à l'écran, vous devez installer matchbox-keyboard, comme suit:
```bash
sudo apt install matchbox-keyboard
```

## Diagnostics

Pour tout problème relié à l'affichage, assurez-vous d'avoir la version la plus à jour de RaspberryPi OS en lancant la commande `sudo apt upgrade`.

Si l'écran n'affiche rien ou si l'affichage contient des lignes ou tressaute, vérifiez les connexions.

Si l'écran est à l'envers, il suffit de changer un paramètre de configuration au démarrage. Ajoutez la ligne suivante dans le fichier `/boot/config.txt`:
```bash
lcd_rotate=2
```
puis redémarrez.

Il est aussi possible de modifier l'orientation de l'écran dans `Preferences -> Screen Configuration -> Layout -> DSI1 -> Orientation`.


