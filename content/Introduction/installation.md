---
title: "Installation"
date: 2023-08-20T17:44:17-04:00
draft: false
weight: 11
---

Dans cette section nous verrons comment réaliser les branchements du *Raspberry Pi* et installer le système d'exploitation *Raspberry Pi OS*.

## Matériel requis

| | |
|:--|--|
| Raspberry Pi 4 Modèle B | ![pi4](/420-314/images/pi4.jpg?width=150px) |
| Clavier et câble USB | ![kbd](/420-314/images/kbd.png?width=150px) |
| Souris | ![souris](/420-314/images/souris.png?width=150px) |
| Alimentation | ![alim](/420-314/images/alim.png?width=150px) |
| Câble HDMI | ![hdmi](/420-314/images/cblhdmi.png?width=150px) |
| Carte SD | ![sdcard](/420-314/images/sdcard.png?width=150px) |

## Assemblage
La carte SD contient le programme [NOOBS](https://github.com/raspberrypi/noobs) qui permet de sélectionner, télécharger puis installer un système d'exploitation compatible avec le Pi. Insérez la carte SD dans le lecteur:
![insertsd](/420-314/images/insertsd.png?width=400px)

Branchez ensuite le clavier, la souris, le câble HDMI sur un écran et en dernier, l'alimentation:
![assemb](/420-314/images/assemb.png?width=400px)

## Installation
Après le démarrage, sélectionnez *Raspberry Pi OS Full (32-bit)* puis cliquez sur **Install**:
![install](/420-314/images/install.png?width=400px)

Vous aurez quelques paramètres de configuration à définir. Entrez les valeurs suivantes:
+ Pays: *Canada* 
+ Timezone: *Eastern*
+ Clavier: *Canadian English*
+ User (si demandé): *pi*
+ Password: *abc-123* (ou autre chose si vous êtes certain de vous en souvenir!)
+ Wifi: *Skip*
+ Updates: *Skip*

{{% notice info "Programme d'installation" %}}
Le programme *NOOBS*, bien qu'il soit se trouve sur la carte SD fournie, n'est plus le logiciel officiel d'installation de *Raspberry Pi OS*: il a été remplacé par [Raspberry Pi Imager](https://www.raspberrypi.com/software/). Si vous devez réinstaller l'OS, utilisez ce dernier.
{{% /notice %}}