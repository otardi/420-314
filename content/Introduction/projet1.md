---
title: "Projet 1"
date: 2023-08-22T18:03:40-04:00
draft: false
weight: 13
---

Notre premier projet a pour but de vérifier si la configuration du Pi a bien été effectuée et si tout est en place pour développer des prototypes en python ou en C incluant des modules du kit *Keyestudio*.

## Matériel requis

| | |
|:--|--|
| Module LED blanche | ![mod1led](/420-314/images/mod1led.png?width=150px) |
| 3 connecteurs F-F | ![3jumpff](/420-314/images/3jumpff.png?width=150px) |

## Connexions
Le module dispose de 3 broches: 
+ `V` pour le courant positif (5v)
+ `G` pour le courant négatif ("ground")
+ `S` pour le signal de contrôle

Ces trois broches doivent être raccordées à des broches compatibles sur le Pi. Il faut donc identifier celles-ci; un excellent outil pour ce faire est [https://pinout.xyz/#](https://pinout.xyz/#).

Nous utiliserons donc les broches suivantes sur le Pi:
+ `2` pour le courant 5v
+ `6` pour le courant négatif
+ `11` pour le contrôle (GPIO-17)
  
![gpioP1](/420-314/images/gpioP1.png?width=400px)
> [Source](https://www.raspberrypi.com/documentation/computers/raspberry-pi.html)

Avant de connecter le module au Pi, **il est préférable d'éteindre celui-ci**. Ceci permet d'éviter que les variations de courant qui peuvent se produire au moment des connexions n'endommagent les circuits fragiles du RaspberryPi.

Une fois les connexions terminées, votre assemblage doit ressembler à ceci:

![p1setup](/420-314/images/p1setup.png?width=400px)

{{% notice info "Conventions" %}}
L'électricité peut parcourir un fil de n'importe quelle couleur, mais il existe des conventions quant aux couleurs des fils qu'on utilise selon le signal qu'ils transportent.
+ Rouge: courant positif
+ Noir: courant négatif
Parfois le vert est aussi utilisé pour le courant négatif lorsqu'il s'agit d'une mise à la terre ("ground")
{{% /notice %}}

Avant de rallumer votre Pi, **vérifiez au moins deux fois** que les fils sont branchés sur les bonnes broches.

## Programme
Les programmes que nous allons tester ont pour effet de faire clignoter la LED à chaque seconde.

#### python
```python
import pigpio
import time

pi = pigpio.pi()
pi.set_mode(17,pigpio.OUTPUT)

while True:
        pi.write(17,1)
        time.sleep(1)
        pi.write(17,0)
        time.sleep(1)
```

#### C
```C
#include <wiringPi.h>
int main (void) 
{
    wiringPiSetup();
    pinMode(0,OUTPUT);
    while(1) 
    {
      digitalWrite(0,HIGH);
      delay(1000);
      digitalWrite(0,LOW);
      delay(1000);
    }
    return 0 ;
}
```
Étant donné que le programme utilise la librairie *wiringPi*, il faut signaler au compilateur `gcc` de lier cette librairie au programme éxécutable qui sera créé. Pour ce faire on utilise l'option `-l` suivie du nom de la librairie.

La commande pour compiler est donc la suivante:
```bash
gcc -Wall projet1.c -o projet1 -lwiringPi
```
{{% notice info "-Wall" %}}
Le compilateur gcc a de nombreuses options. Parmi celles-ci, l'option `-W` permet de contrôler la quantité de messages d'erreurs ("warnings") qui sera affichée. `-Wall` affiche tous les messages, ce qui est utile lorsqu'il y a des problèmes lors de la compilation.
{{% /notice %}}

Pour exécuter le programme:
```bash
./projet1 
```


  

