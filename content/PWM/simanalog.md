---
title: "Simulation de signaux analogiques"
date: 2023-10-03T20:34:43-04:00
draft: false
weight: 41
---

La modulation à largeur d'impulsion (MLI, ou en anglais PWM pour *Pulse Width Modulation*), est une technique très utilisée dans le domaine de l'électronique. Elle permet de contrôler la puissance délivrée à une composante électrique en modulant la durée des impulsions électriques. On s'en sert pour simuler un signal analogique, ou encore pour contrôler des servo-moteurs.

Dans cette section nous verrons comment fonctionne cette technique et comment la librairie ``pigpio`` permet d'utiliser la MLI pour simuler des signaux analogiques.

## Fonctionnement
Comme nous l'avons vu précédemment le Raspberry Pi ne peut envoyer que des signaux numériques. Avec les broches GPIO, il y a deux états possibles: soit elles envoient un courant, soit elles n'en envoient pas. Il n'est pas possible de faire varier le voltage sur celles-ci de façon continue.

Il en est de même pour la réception de signaux: les broches GPIO ne peuvent lire que les valeurs 0 ou 1.

Lorsqu'on dispose de senseurs qui envoient des signaux analogiques, comme un potentiomètre ou un détecteur de luminosité, il faut utiliser un convertisseur pour traduire le signal analogique en valeurs numériques. Mais cette traduction n'est possible que pour les signaux qui vont ***vers*** le Raspberry Pi. 

La MLI permet de simuler des signaux analogiques envoyés ***à partir*** d'un Raspberry Pi vers un actuateur.

Le principe est assez simple. Si on a un courant de 5V (par exemple) et qu'on alterne ce courant rapidement avec un signal de 0V, la tension dans le circuit sera de 2.5V, car la moyenne entre les deux valeurs 0V et 5V est de 2.5V.

Dans cet exemple, on suppose que la durée de l'impulsion de 5V est égale à la durée du signal de 0V: c'est pourquoi la moyenne est de 2.5V. Mais cette moyenne dépend des durées relatives entre le signal et l'absence de signal. Pour une **période** de 100ms, une impulsion de 50ms correspond à 50% de la période; l'effet sur le voltage sera proportionnel: pour un courant de 5V, le résultat sera un tension de 50% de 5V, donc 2.5V. Pour la même période, une impulsion de 30ms correspond à 30% de la période, donc donnera un courant de 1.5V (30% de 5V); etc.

![pwm1](/420-314/images/pwm1.png)

On appelle *cycle de travail* (en anglais, "duty cycle") cette proportion entre la durée de l'impulsion et la durée de la période. Donc pour une tension de 5V, un cycle de travail de 20% donnera un signal de 1V (20% de la tension de référence de 5V), et ainsi de suite. Un élément important: la durée de la période n'a pas beaucoup d'importance ici, ce qui compte c'est la valeur du *duty cycle*.

#### Quelques questions:
1. Pour une tension de 5V, un cycle de travail de 25% donnera quel voltage?
2. Pour une tension de 5V, une période de 200ms et un cycle de travail de 40ms, quel sera le voltage?
3. Pour une tension de 3.3V, une période de 150ms et un cycle de travail de 90ms, quel sera le voltage?
4. Pour une tension de 5V et une période de 100ms, combien de ms doit avoir le cycle de travail pour obtenir un signal de 3V?
5. Pour une tension de 3.3V et une période de 20ms, combien de ms doit avoir le cycle de travail pour obtenir un signal de 1V?
{{% expand "Solutions" %}}
1. 1.25V (25% de 5V)
2. 1V (40ms / 200ms = 20%; 20% de 5V = 1V)
3. 1.98V (90ms / 150ms = 60%; 60% de 3.3V = 1.98V)
4. 60ms (3V / 5V = 60%; 60% de 100ms = 60ms)
5. Environ 6ms (1V / 3.3V = 30.3%; 30.3% de 20ms = 6.06ms)
{{% /expand %}}


## Exemple 

Dans ce programme on allume graduellement le module LED.

On utilise la fonction `set_PWM_dutycycle()` du module *pigpio*. Elle prend 2 paramètres:
+ Le numéro GPIO de la broche
+ La valeur du cycle de travail.

Ici le **dutycycle** n'est pas un pourcentage mais plutôt une valeur comprise entre 0 et 255. Sur le Raspberry Pi, ils correspondent respectivement à 0V et 3.3V car le courant électrique qu'on peut envoyer via une broche GPIO a une tension de 3.3V.

### Connexions
+ LED: S sur broche 33 (GPIO13)
+ LED: V sur courant 3.3V
+ LED: G sur "ground"

### Programme
```python
import pigpio
import time

LED = 13
MAX = 255 # La valeur maximale que set_PWM_dutycycle() accepte

pi = pigpio.pi()
pi.set_mode(LED,pigpio.OUTPUT)

cycle = 0 # Variable qui correspond à l'intensité (simulée) du voltage

while cycle < MAX:
    cycle += 1
    pi.set_PWM_dutycycle(LED,cycle)
    time.sleep(0.01)

pi.write(LED,0)
```

## Exercice 1
Modifiez le code de l'exemple précédent pour faire "pulser" la LED, c'est-à-dire qu'elle est d'abord éteinte, s'allume graduellement, puis s'éteint graduellement, indéfiniment, à raison d'un cycle par seconde.

```python
import pigpio
import time

LED = 13
MAX = 255 # La valeur maximale que set_PWM_dutycycle() accepte

pi = pigpio.pi()
pi.set_mode(LED,pigpio.OUTPUT)

cycle = 0 # Variable qui correspond à l'intensité (simulée) du voltage

try:
    while True:
        while cycle < MAX:
            cycle += 1
            pi.set_PWM_dutycycle(LED,cycle)
            time.sleep(1/MAX)
        while cycle > 0:
            cycle -= 1
            pi.set_PWM_dutycycle(LED,cycle)
            time.sleep(1/MAX)
except KeyboardInterrupt:
    pi.write(LED,0)
```


## Exercice 2
Utilisez un potentiomètre (le module Keystudio ["analog rotation sensor"](/420-314/images/ars.png?width=300px)) pour contrôler la luminosité de la LED.

### Connexions
+ ADS1115: VDD sur courant 3.3V
+ ADS1115: GND sur "ground"
+ ADS1115: SCL sur broche GPIO3
+ ADS1115: SDA sur broche GPIO2
+ "Rotation sensor": `S` sur broche A0 du ADS1115
+ "Rotation sensor": `V` sur courant 3.3V
+ "Rotation sensor": `G` sur "ground"
+ LED: `S` sur broche 33 (GPIO13)
+ LED: `V` sur courant 3.3V
+ LED: `G` sur "ground"
  

### Module *CircuitPython*

Attention: les exemples suivants utilisent un module plus récent pour 
le convertisseur ADS1115. Cette nouvelle version est généralement plus stable, mais les classes et les méthodes sont différentes. 

Pour l'installer, lancez la commande suivante:
```bash
sudo pip3 install adafruit-circuitpython-ads1x15
```
Puis redémarrez l'interface I2C.

Vous pouvez tester le module avec le programme suivant:
```python
import time
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

SCL = 3
SDA = 2
GAIN = 1

#Initialiser le protocole I2C sur les broches 2 et 3
i2cBus = busio.I2C(SCL, SDA) 
#Initialiser un objet qui associe le convertisseur à ce protocole
ads = ADS.ADS1115(i2cBus, GAIN) 

# Initialiser les broches en mode Input analogue
a0 = AnalogIn(ads, 0)
a1 = AnalogIn(ads, 1)
a2 = AnalogIn(ads, 2)
a3 = AnalogIn(ads, 3)

while True:
    print(a0.value, a1.value, a2.value, a3.value)
    time.sleep(0.1)
```
[Référence](https://docs.circuitpython.org/projects/ads1x15/en/latest/index.html)

### Programme

```python
import pigpio
import time
import busio
import adafruit_ads1x15.ads1115 as ADC
from adafruit_ads1x15.analog_in import AnalogIn

SCL = 3 # Num GPIO de broche SCL
SDA = 2 # Num GPIO de broche SDA
GAIN = 1
LED = 13 # Num GPIO de broche PWM
MAX = 26300 # La valeur maximale du potentiometre

# Init module ADC
i2cBus = busio.I2C(SCL, SDA)
adc = ADC.ADS1115(i2cBus, GAIN)

# Init GPIO
pi = pigpio.pi()
pi.set_mode(LED,pigpio.OUTPUT)

cycle = 0 

try:
    while True:
        a0 = AnalogIn(adc, 0)
        cycle = a0.value * 255/MAX
        pi.set_PWM_dutycycle(LED,cycle)
        time.sleep(0.01)

except KeyboardInterrupt:
    pi.write(LED,0)
```


## Exercice 3
Contrôlez la LED RGB avec le potentiomètre. Si on suppose que le potentiomètre va de 0% à 100%, votre programme doit faire correspondre:
+ 0% à rouge
+ 25% à jaune
+ 50% à vert
+ 75% à cyan
+ 100 à bleu

### Programme

```python
import pigpio
import time
import busio
import adafruit_ads1x15.ads1115 as ADC
from adafruit_ads1x15.analog_in import AnalogIn

def rgb(p,dValeurs):
    for k in dValeurs.keys():
        try:
            cycle = 255 - dValeurs[k]
            p.set_PWM_dutycycle(k,cycle)
        except:
            print(dValeurs)

SCL = 3 # Num GPIO de broche SCL
SDA = 2 # Num GPIO de broche SDA
R,G,B = 17,27,22 #Broches GPIO pour LED
GAIN = 1
MAX = 26300 # La valeur maximale du potentiometre

# Intervalles
QUART=MAX*0.25
MOITIE=MAX*0.5
TQUARTS=MAX*0.75

# Init module ADC
i2cBus = busio.I2C(SCL, SDA)
adc = ADC.ADS1115(i2cBus, GAIN)

# Init GPIO
pi = pigpio.pi()
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

d ={}
a0 = 0

try:
    while True:
        a0 = AnalogIn(adc, 0).value
        if a0 < QUART:
            d[R]=255
            d[G]=abs(int(a0/QUART*255))
            d[B]=0
        elif QUART <= a0 < MOITIE:
            d[R]=abs(int(255-((a0-QUART)/QUART*255)))
            d[G]=255
            d[B]=0
        elif MOITIE <= a0 < TQUARTS:
            d[R]=0
            d[G]=255
            d[B]=abs(int((a0-MOITIE)/QUART*255))
        elif TQUARTS <= a0 < MAX:
            d[R]=0
            d[G]=int(255-((a0-TQUARTS)/QUART*255))
            d[B]=255

        rgb(pi,d)

except KeyboardInterrupt:
    pi.write(R,1)
    pi.write(G,1)
    pi.write(B,1)
```