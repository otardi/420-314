---
title: "Projet 2"
date: 2023-08-30T18:06:20-04:00
draft: false
weight: 23
---

Le projet 2 se compose de 3 petits circuits qui consistent à utiliser une plaquette de prototypage et des composantes électroniques avec le Raspberry Pi. 

-----------------
## Circuit 1 - 1 LED clignotante

Dans ce circuit il s'agit de faire clignoter une LED simple en contrôlant l'alimentation par le Raspberry Pi. Attention cependant: dans le projet 1, nous avons connecté un **module LED** sur 3 broches GPIO (courant positif, courant négatif et signal). Ici, on a une LED toute simple qui ne doit être connectée qu'à un courant positif et un courant négatif.

Dans le Raspberry Pi, lorsqu'on envoit un courant sur une broche GPIO comme on le faisait dans le projet 1, sa tension est de **3.3V**. Donc on peut utiliser une broche GPIO pour alimenter la LED avec un courant de 3.3V.

### Schéma

![circ2a](/420-314/images/circ2a.png?width=400px)

### Matériel requis

| | |
|:--|--|
| LED 20mA | ![redled](/420-314/images/redled.png?width=150px) |
| 2 connecteurs M-F | ![2jumpmf](/420-314/images/2jumpmf.png?width=150px) |
| 2 connecteurs M-M | ![2jumpmm](/420-314/images/2jumpmm.png?width=150px) |
| 1 résistance 200Ω | ![res200](/420-314/images/res200.png?width=150px) |

### Connexions

![proj2a](/420-314/images/proj2a.png?width=400px)

Dans la plaquette, le courant positif doit provenir d'une broche GPIO du Pi (par exemple la broche 13), et le courant négatif est envoyé sur une broche *Ground* (par exemple la broche 14).

### Programme
Vous pouvez utiliser le même programme python que pour le projet 1, mais faites bien attention aux ports GPIO utilisés dans le programme. Dans l'exemple suivant c'est le port GPIO 27 qui est utilisé.

```python
import pigpio
import time

pi = pigpio.pi()
pi.set_mode(27,pigpio.OUTPUT)

while True:
        pi.write(27,1)
        time.sleep(1)
        pi.write(27,0)
        time.sleep(1)
```

-----------------
## Circuit 2 - 1 LED activée par un bouton

Dans ce circuit nous allons utiliser le Pi pour envoyer un courant constant, mais nous ajoutons un bouton pour allumer la LED.

### Schéma

![circ2b](/420-314/images/circ2b.png?width=400px)

### Matériel requis

| | |
|:--|--|
| LED 20mA | ![redled](/420-314/images/redled.png?width=150px) |
| 2 connecteurs M-F | ![2jumpmf](/420-314/images/2jumpmf.png?width=150px) |
| 2 connecteurs M-M | ![2jumpmm](/420-314/images/2jumpmm.png?width=150px) |
| 1 résistance 200Ω | ![res200](/420-314/images/res200.png?width=150px) |
| 1 bouton | ![1btn](/420-314/images/1btn.png?width=150px) |

### Connexions

![proj2b](/420-314/images/proj2b.png?width=400px)

### Programme

Puisqu'on ne veut plus que la LED clignote, le courant doit être constant. La boucle `while` doit donc uniquement contenir une instruction qui envoie du courant à la broche GPIO désignée:

```python
import pigpio
import time

pi = pigpio.pi()
pi.set_mode(27,pigpio.OUTPUT)

while True:
        pi.write(27,1)
```
-----------------
## Circuit 3 - 2 LED activées par 2 boutons

Dans ce circuit chaque LED est allumée par un bouton. Attention: le circuit est parallèle, ce qui fait que l'alimentation des deux LED sont indépendantes. Ceci permet par ailleurs d'utiliser une seule résistance si elle est judicieusement placée.

### Schéma

![circ2c](/420-314/images/circ2c.png?width=400px)

### Matériel requis

| | |
|:--|--|
| 2 LED 20mA | ![2redled](/420-314/images/2redled.png?width=150px) |
| 2 connecteurs M-F | ![2jumpmf](/420-314/images/2jumpmf.png?width=150px) |
| 4 connecteurs M-M | ![4jumpmm](/420-314/images/4jumpmm.png?width=150px) |
| 1 résistance 200Ω | ![res200](/420-314/images/res200.png?width=150px) |
| 2 boutons | ![2btn](/420-314/images/2btn.png?width=150px) |

### Connexions

![proj2c](/420-314/images/proj2c.png?width=400px)

### Programme

Le programme est identique à celui du circuit 2.


