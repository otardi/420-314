+++
chapter = true
pre = "<b>6. </b>"
title = "I2C"
weight = 60
+++

### Module 6

# I2C

Le protocole de communication **I2C**, ou *Inter-Integrated Circuit*, est un standard de communication bidirectionnel qui permet de connecter des composants électroniques. Il permet à plusieurs dispositifs, tels que capteurs, actuateurs et microcontrôleurs, de communiquer entre eux à l'aide de deux lignes de transmission : **SDA** (*Serial Data Line*) pour les données et **SCL** (*Serial Clock Line*) pour l'horloge, qui sert à synchroniser les messages dans la communication. Ce protocole est souvent utilisé dans des applications où plusieurs composants doivent échanger des données. I2C offre une solution efficace pour réduire le nombre de connexions nécessaires, ce qui simplifie les branchements tout en facilitant l'expansion des fonctionnalités d'un système.

