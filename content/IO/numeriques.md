---
title: "Signaux numériques"
date: 2023-09-13T16:28:57-04:00
draft: false
weight: 31
---

## Types de signaux

En électronique, les composantes peuvent envoyer et recevoir des signaux **numériques** (ou "digitaux") ou des signaux **analogiques**. 

Les signaux sont transportés par un courant électrique: par exemple dans le *RaspberryPi*, les broches GPIO utilisent un courant de 3.3V; dans le cas d'un *Arduino UNO*, les broches `DIGITAL OUT` utilisent un courant de 5V. 

Les signaux numériques sont **binaires**, c'est-à-dire qu'ils ne peuvent prendre que deux valeurs:  "éteint" ou "allumé" (ou 0/1, HAUT/BAS, etc.). 

Les signaux analogiques sont **continus** et peuvent prendre une gamme infinie de valeurs à l'intérieur d'une plage donnée. Par exemple, une tension analogique peut varier de manière continue entre 0V et 5V (0.5V, 2V, 3.344V, etc.).

Le *RaspberryPi* est conçu pour gérer des *signaux numériques uniquement*. Cependant, de nombreux senseurs et actuateurs utilisent des signaux analogiques: par exemple, un senseur de température peut envoyer une infinité de valeurs entre deux extrêmes, et pas uniquement "0" ou "1". Pour gérer les signaux analogiques sur un *RaspberryPi*, il nous faudra donc utiliser une composante spéciale: un convertisseur analogique-numérique (ou ADC, "Analog to Digital Converter").

Dans cette section, nous verrons comment utiliser un senseur numérique (un bouton) pour contrôler un actuateur numérique (une LED RGB).

{{% notice info "Référence utile" %}}
Dans cette section nous utiliserons deux modules *Keystudio 48 in 1 (KS0522)*. Pour tous les exemples, et tous les autres projets utilisant le même kit, il serait utile de vous référer à la [documentation du module KS0522](https://ks0522-keyestudio-48-in-1-sensor-kit.readthedocs.io/en/latest/KS0522.html). 
{{% /notice %}}

## Bouton-poussoir numérique

Le bouton envoit un signal correspondant à `0` lorsqu'on l'appuie et `1` lorsqu'on le relâche. On peut utiliser ces signaux pour contrôler l'exécution d'un programme.

### Matériel requis

| | |
|:--|--|
| Module "Push Button" | ![kspshbut](/420-314/images/kspshbut.png?width=150px) |
| 3 connecteurs F-F | ![3jumpff](/420-314/images/3jumpff.png?width=150px) |

### Connexions

Dans cet exemple nous utiliserons les broches suivantes du *RaspberryPi*:
+ broche 1 (courant 3,3V)
+ broche 9 (courant négatif)
+ broche 16 (GPIO23)
  
### Programme `bouton1.py`

```python
import pigpio
import time

pi = pigpio.pi()
pi.set_mode(23,pigpio.INPUT)

while True:
        print(pi.read(23),end="")
```

Lorsque vous exécutez ce programme, une série de "1" s'affiche en continu, et "0" s'affiche lorsque vous cliquez le bouton.

{{% notice info "Exercices" %}}
1. Faites un programme qui affiche "0" une seule fois lorsqu'on clique le bouton, et qui affiche "1" une seule fois lorsqu'on le relâche.
2. Faites un programme qui affiche seulement "clic" chaque fois qu'on clique sur le bouton.
{{% /notice %}}

{{% expand "Solution 1." %}}
```python
import pigpio

pi = pigpio.pi()
pi.set_mode(23,pigpio.INPUT)

dernier = 1
while True:
    signal = pi.read(23)
    if signal != dernier: 
        print(signal)
    dernier = signal
```
{{% /expand %}}

{{% expand "Solution 2." %}}
```python
import pigpio

pi = pigpio.pi()
pi.set_mode(23,pigpio.INPUT)

dernier = 1
while True:
    signal = pi.read(23)
    if signal != dernier and signal == 0: 
        print("clic")
    dernier = signal
```
{{% /expand %}}


## LED RGB

Les LED RGB ("Light Emitting Diode Red, Green, Blue") sont des diodes électroluminescentes qui peuvent émettre de la lumière dans différentes couleurs en mélangeant la lumière rouge, verte et bleue à des intensités variables. 

Dans n'importe quel type de diode, on nomme **anode** la broche *positive* et **cathode** la broche *négative*. Par exemple pour une LED simple:

![ledpoles](/420-314/images/ledpoles.png?width=300px)

Dans le cas d'une LED RGB, les choses sont un peu différentes. Le fait qu'elles aient une source d'alimentation pour 3 couleurs permet de regrouper des broches.

Les LED RGB à **anode commune** et à **cathode commune** sont deux configurations différentes pour ce type de LED; elles diffèrent par la manière dont elles sont connectées électriquement:

![rgbtypes](/420-314/images/rgbtypes.png?width=400px)

### Cathode commune
Dans une *LED RGB à cathode commune*, les cathodes (-) de chaque LED interne sont connectées ensemble et sortent sous forme d'une seule broche commune (la LED RGB dans *TinkerCAD* est de ce type).

Pour allumer une couleur spécifique sur une LED RGB à cathode commune, il faut connecter la broche de cette couleur à un courant positif et connecter la cathode commune au courant négatif (*ground*).

### Anode commune
Dans une *LED RGB à anode commune*, les anodes (+) de chaque LED interne (rouge, verte et bleue) sont connectées ensemble et sortent sous forme d'une seule broche commune.

Pour allumer une couleur spécifique sur une LED RGB à anode commune, il faut connecter l'anode commune à un courant positif et connecter la ou les broches de la couleur souhaitée au courant négatif (*ground*).

> La LED RGB du module Keystudio KS0522 est une LED à anode commune. Il faut donc envoyer un courant positif sur l'anode et les broches qu'on veut garder éteintes, et un courant négatif sur les broches qu'on veut allumer.

### Matériel requis

| | |
|:--|--|
| Module "RGB LED" | ![ksrgbled](/420-314/images/ksrgbled.png?width=150px) |
| 4 connecteurs F-F | ![3jumpff](/420-314/images/3jumpff.png?width=150px) |

### Connexions

Dans cet exemple nous utiliserons les broches suivantes du *RaspberryPi*:
+ broche 17 (courant 3,3V)
+ broche 11 (GPIO17, rouge)
+ broche 13 (GPIO27, vert)
+ broche 15 (GPIO22, bleu)
  
### Programme `rgb1.py`

Le programme suivant aura pour effet d'allumer la LED selon le cycle suivant: 1 seconde en rouge, 1 seconde en vert et 1 seconde en bleu.

```python
import pigpio
import time

R,G,B=17,27,22

pi = pigpio.pi()
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

# Allumer R, éteindre G et B
pi.write(R,0)
pi.write(G,1)
pi.write(B,1)
time.sleep(1)

# Allumer G, éteindre R et B
pi.write(R,1)
pi.write(G,0)
pi.write(B,1)
time.sleep(1)

# Allumer B, éteindre R et G
pi.write(R,1)
pi.write(G,1)
pi.write(B,0)
time.sleep(1)

# Éteindre tout
pi.write(R,1)
pi.write(G,1)
pi.write(B,1)
```

{{% notice info "Exercices" %}}
1. Faites un programme qui allume la LED en rouge lorsqu'on appuie le bouton et l'éteint lorsqu'on le relâche.
2. Faites un programme qui allume la LED en rouge lorsqu'on clique une fois et l'éteint lorsqu'on clique une deuxième fois.
3. Faites un programme qui cycle entre les 3 couleurs rouge, vert et bleu, où la couleur change à chaque clic.
4. Si on mélange les couleurs R, G et B, on obtient ces couleurs:
   + R + G = jaune
   + R + B = violet
   + G + B = cyan
   + R + G + B = blanc
   
Faites un programme similaire au précédent, mais qui cycle entre rouge, jaune, vert, cyan, bleu, violet, blanc.
{{% /notice %}}

{{% expand "Solution 1." %}}
```python
import pigpio

R,G,B = 17,27,22
btn = 23

def allumer():
    pi.write(R,0)
    pi.write(G,1)
    pi.write(B,1)

def eteindre():
    for pin in [R,G,B]: pi.write(pin,1)

pi = pigpio.pi()

pi.set_mode(btn,pigpio.INPUT)
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

dernier = 1 
while True:
    signal = pi.read(btn)
    if signal != dernier:
        if signal == 0: 
            allumer()
        else: eteindre()
        
    dernier = signal
```
{{% /expand %}}
{{% expand "Solution 2." %}}
```python
import pigpio
import time

R,G,B = 17,27,22
btn = 23

def allumer():
    pi.write(R,0)
    pi.write(G,1)
    pi.write(B,1)

def eteindre():
    for pin in [R,G,B]: pi.write(pin,1)

def changerEtat(e):
    if e == "off":
        allumer()
        return "on"
    else:
        eteindre()
        return "off"

pi = pigpio.pi()

pi.set_mode(btn,pigpio.INPUT)
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

eteindre()
etat = "off"

dernier = 1 
while True:
    signal = pi.read(btn)
    if signal != dernier:
        if signal == 0:
            etat = changerEtat(etat)
    dernier = signal
    time.sleep(0.1)
```
{{% /expand %}}
{{% expand "Solution 3." %}}
```python
import pigpio
import time

R,G,B = 17,27,22
btn = 23

def rouge():
    pi.write(R,0)
    pi.write(G,1)
    pi.write(B,1)

def vert():
    pi.write(R,1)
    pi.write(G,0)
    pi.write(B,1)

def bleu():
    pi.write(R,1)
    pi.write(G,1)
    pi.write(B,0)

def eteindre():
    for pin in [R,G,B]: pi.write(pin,1)

def changerEtat(e):
    if e == "off" or e == "b":
        rouge()
        return "r"
    elif e == "r":
        vert()
        return "v"
    elif e == "v":
        bleu()
        return "b"

pi = pigpio.pi()

pi.set_mode(btn,pigpio.INPUT)
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

eteindre()
etat = "off"

dernier = 1 
while True:
    signal = pi.read(btn)
    if signal != dernier:
        if signal == 0:
            etat = changerEtat(etat)
    dernier = signal
    time.sleep(0.1)
```
{{% /expand %}}
{{% expand "Solution 4." %}}
```python
import pigpio
import time

R,G,B = 17,27,22
btn = 23

def rouge():
    pi.write(R,0)
    pi.write(G,1)
    pi.write(B,1)

def vert():
    pi.write(R,1)
    pi.write(G,0)
    pi.write(B,1)

def bleu():
    pi.write(R,1)
    pi.write(G,1)
    pi.write(B,0)

def jaune():
    pi.write(R,0)
    pi.write(G,0)
    pi.write(B,1)

def mauve():
    pi.write(R,0)
    pi.write(G,1)
    pi.write(B,0)

def cyan():
    pi.write(R,1)
    pi.write(G,0)
    pi.write(B,0)

def eteindre():
    for pin in [R,G,B]: pi.write(pin,1)

def changerEtat(e):
    if e == "off" or e == "m":
        rouge()
        return "r"
    elif e == "r":
        jaune()
        return "j"
    elif e == "j":
        vert()
        return "v"
    elif e == "v":
        cyan()
        return "c"
    elif e == "c":
        bleu()
        return "b"
    elif e == "b":
        mauve()
        return "m"
    

pi = pigpio.pi()

pi.set_mode(btn,pigpio.INPUT)
pi.set_mode(R,pigpio.OUTPUT)
pi.set_mode(G,pigpio.OUTPUT)
pi.set_mode(B,pigpio.OUTPUT)

eteindre()
etat = "off"

dernier = 1 
while True:
    signal = pi.read(btn)
    if signal != dernier:
        if signal == 0:
            etat = changerEtat(etat)
    dernier = signal
    time.sleep(0.1)
```
{{% /expand %}}



