---
title: "Projet 3"
date: 2023-09-26T23:08:37-04:00
draft: false
weight: 33
---

Le projet 3 se compose de 4 étapes. 

## Étape 1 - Détecter un seuil de luminosité

À l'aide d'un module analogique détectant la lumière ambiante, vous devez afficher un message selon la quantité de lumière perçue. Plus spécifiquement:
+ Afficher "Allumer la lumière" une seule fois quand la luminosité descend sous la moitié de la luminosité maximale;
+ Afficher "Étendre la lumière" une seule fois quand la luminosité monte au-delà la moitié de la luminosité maximale;

### Matériel requis
+ Module [KeyStudio TEMT6000](/420-314/images/temt6000.png)
+ Convertisseur analogique-digital [ADS1115](/420-314/images/adcchip.png)
+ 7 connecteurs F-F

### Connexions
+ ADS1115: VDD sur broche `3v3` du Pi
+ ADS1115: GND sur broche `Ground` du Pi
+ ADS1115: SCL sur broche 5 du Pi
+ ADS1115: SDA sur broche 3 du Pi
+ TEMT6000: `S` sur broche A0 du ADS1115
+ TEMT6000: `V` sur broche `3v3` du Pi
+ TEMT6000: `G` sur broche `Ground` du Pi


### Programme

Nommez votre programme *lumi.py*, et utilisez une valeur de GAIN de 1.

```python
import time
import Adafruit_ADS1x15

adc = Adafruit_ADS1x15.ADS1115()

# Gain par defaut pour -4v à +4v
GAIN = 1
# Valeur numérique maximale mesurée sur le capteur de luminosité
MAX = METTEZ ICI LA VALEUR MAX OBTENUE SUR VOTRE CAPTEUR 
lastState = 0
thisState = 0

while True:
    a0 = adc.read_adc(0, gain=GAIN)

    # Si la valeur est inférieure à la moitié de la luminosité,
    # variable d'état = "lumière ambiante faible"
    if a0 < MAX/2:
        thisState = 0
    else: # Sinon variable d'état = "lumière ambiante forte"
        thisState = 1

    # Si l'état actuel est "lumière ambiante forte" et le dernier 
    # état était "lumière ambiante faible", afficher "Éteindre"
    if thisState == 1 and lastState == 0:
        print("Éteindre la lumière")
    elif thisState == 0 and lastState == 1:
        print("Allumer la lumière")

    lastState = thisState 

    time.sleep(0.01)
```


## Étape 2 - Accélérer un affichage en fonction de la luminosité
Afficher à l'écran un message de plus en plus rapidement selon la quantité de lumière. Plus précisément: à la quantité minimum de lumière, on affiche le message 1 fois par seconde. À la quantité maximum de lumière, on affiche le message 100 fois par seconde.

### Matériel requis
(similaire à l'étape 1)
+ Module [KeyStudio TEMT6000](/420-314/images/temt6000.png)
+ Convertisseur analogique-digital [ADS1115](/420-314/images/adcchip.png)
+ 7 connecteurs F-F

### Connexions
(similaire à l'étape 1)
+ ADS1115: VDD sur broche `3v3` du Pi
+ ADS1115: GND sur broche `Ground` du Pi
+ ADS1115: SCL sur broche 5 du Pi
+ ADS1115: SDA sur broche 3 du Pi
+ TEMT6000: `S` sur broche A0 du ADS1115
+ TEMT6000: `V` sur broche `3v3` du Pi
+ TEMT6000: `G` sur broche `Ground` du Pi

### Programme

```python
import time
import Adafruit_ADS1x15

adc = Adafruit_ADS1x15.ADS1115()

GAIN = 1
MAX = 1250

while True:
    a0 = adc.read_adc(0, gain=GAIN)
    delai = 1 - a0 / MAX 
    try:
        print(delai)
        time.sleep(delai)
    except ValueError:
        pass
```



## Étape 3 - Allumer une lumière lorsque la luminosité baisse

Ce projet est similaire à celui de l'étape 1, mais au lieu d'afficher un message à l'écran c'est une LED que vous devez allumer ou éteindre à partir de votre programme.

### Matériel requis

+ Module [KeyStudio TEMT6000](/420-314/images/temt6000.png)
+ Module [KeyStudio LED](/420-314/images/modledblanc.png)
+ Convertisseur analogique-digital [ADS1115](/420-314/images/adcchip.png)
+ 5 connecteurs M-F
+ 7 connecteurs M-M

### Connexions

Dans ce projet, vous devez utiliser la source de courant 3.3V pour 3 composantes. Il faut donc utiliser la plaquette de prototypage pour partager le courant 3.3V du Pi à ces 3 modules. 

+ ADS1115: VDD sur courant 3.3V
+ ADS1115: GND sur "ground"
+ ADS1115: SCL sur broche 5 du Pi
+ ADS1115: SDA sur broche 3 du Pi
+ TEMT6000: `S` sur broche A0 du ADS1115
+ TEMT6000: `V` sur courant 3.3V
+ TEMT6000: `G` sur "ground"
+ LED: S sur broche 37 (GPIO26)
+ LED: V sur courant 3.3V
+ LED: G sur "ground"

### Programme

## Étape 4 - Faire clignoter une LED en fonction de la luminosité
Ce projet est similaire à celui de l'étape 2, mais au lieu d'afficher un message à l'écran c'est une LED que vous devez faire clignoter à partir de votre programme.

### Matériel requis

+ Module [KeyStudio TEMT6000](/420-314/images/temt6000.png)
+ Module [KeyStudio LED](/420-314/images/modledblanc.png)
+ Convertisseur analogique-digital [ADS1115](/420-314/images/adcchip.png)
+ 5 connecteurs M-F
+ 7 connecteurs M-M

### Connexions
(similaire à l'étape 3)
+ ADS1115: VDD sur courant 3.3V
+ ADS1115: GND sur "ground"
+ ADS1115: SCL sur broche 5 du Pi
+ ADS1115: SDA sur broche 3 du Pi
+ TEMT6000: `S` sur broche A0 du ADS1115
+ TEMT6000: `V` sur courant 3.3V
+ TEMT6000: `G` sur "ground"
+ LED: S sur broche 37 (GPIO26)
+ LED: V sur courant 3.3V
+ LED: G sur "ground"

### Programme

```python
import time, pigpio
import Adafruit_ADS1x15

GAIN = 1
MAX = 1250
delai = 1
ledpin = 26

adc = Adafruit_ADS1x15.ADS1115()
pi = pigpio.pi()
pi.set_mode(ledpin,pigpio.OUTPUT)

while True:
    a0 = adc.read_adc(0, gain=GAIN)
    delai = 1 - a0 / MAX
    try:
        pi.write(ledpin,1)
        time.sleep(delai/2)
        pi.write(ledpin,0)
        time.sleep(delai/2)
    except ValueError:
        pass
```
