---
title: "Signaux analogiques"
date: 2023-09-16T20:50:55-04:00
draft: false
weight: 32
---

Dans cette section, nous verrons comment utiliser un convertisseur analogique-digital pour traduire les signaux d'un senseur analogique afin de les rendre utilisables par un RaspberryPi.

Nous utiliserons le convertisseur ADS1115 de *Adafruit*:

![adcchip](/420-314/images/adcchip.png?width=300px)

Tous les senseurs analogiques qu'on souhaite utiliser doivent se connecter sur ce convertisseur. Pour récupérer le signal qui provient des senseurs, le Raspberry Pi doit lui aussi se connecter sur le convertisseur.

La librairie I2C doit être utilisée pour pouvoir interpréter ces signaux dans un programme python.

#### Documentation
https://cdn-learn.adafruit.com/downloads/pdf/adafruit-4-channel-adc-breakouts.pdf

## Connexions du convertisseur ADS1115 sur RaspberryPi
+ VDD sur 3.3V
+ GND sur Ground
+ SCL sur I2C1 SCL
+ SDA sur I2C1 SDA

## Installer le module sur le RaspberryPi
À partir d'une ligne de commande, vous devrez importer le code source de la librairie sur votre Pi puis lancer l'installation. Placez-vous dans le répertoire `/home/pi` puis lancez les commandes suivantes:
```bash
git clone https://github.com/adafruit/Adafruit_Python_ADS1x15.git
cd Adafruit_Python_ADS1x15/
sudo python3 setup.py install
```

## Activer interface I2C
Pour activer l'interface *i2c*, il faut utiliser la commande `raspi-config` comme suit:
```bash
sudo raspi-config
```

## Tester la communication
Vous pouvez tester si votre Pi peut communiquer avec les capteurs en exécutant ce script Python:

*python testADC.py*:
```python
import time
import Adafruit_ADS1x15

adc = Adafruit_ADS1x15.ADS1115()
GAIN = 1

while True:
    a0 = adc.read_adc(0, gain=GAIN)
    a1 = adc.read_adc(1, gain=GAIN)
    a2 = adc.read_adc(2, gain=GAIN)
    a3 = adc.read_adc(3, gain=GAIN)
    print(a0,a1,a2,a3)
    time.sleep(0.5)
```
Vous devriez voir 4 valeurs s'afficher à chaque demi-seconde, comme dans l'exemple suivant:
```bash
pi@prof:~ $ python3 testADC.py 
7050 7470 7286 7458
7059 7446 7293 7401
7602 7288 7461 7258
7660 7285 7453 7267
7249 7422 7292 7460
7006 7475 7288 7448
7223 7369 7459 7265
```

## Utiliser un senseur analogique
Dans cet exemple nous allons utiliser le module de potentiomètre du kit Keystudio ("analog rotation sensor"):

![ars](/420-314/images/ars.png?width=300px)

Sur le convertisseur, il reste 6 broches inutilisées; parmi celles-ci 4 peuvent être utilisées pour recevoir des signaux analogiques: il s'agit de A0, A1, A2 et A3. Nous devons donc brancher une de ces 4 broches à celle qui correspond au signal envoyé par le module (la broche "S").

Le potentiomètre doit donc être connecté comme suit:

+ V : broche **5v** du RaspberryPi
+ G : broche **Ground** du RaspberryPi
+ S : broche **A0** du convertisseur ADC

Lancez le programme *testADC.py* et observez ce qui se passe lorsque vous tournez le bouton.

## Interprétation des signaux analogiques
Lorsqu'on tourne le potentiomètre, le voltage envoyé par celui-ci vers le convertisseur varie. Le convertisseur ADC reçoit ce signal électrique et le traduit en valeur numérique, puis l'envoit vers le RaspberryPi. 

Ce signal envoyé par la puce ADC est une valeur sur 16 bits, et qui peut donc aller de -32768 à +32767; ce n'est pas une mesure directe du voltage. Dépendant du senseur, les caractéristiques du signal envoyé peuvent être différentes. C'est pourquoi on peut utiliser un paramètre (la variable `GAIN`) pour modifier la manière dont la puce ADC convertit le voltage en valeurs 16bit.

Le tableau suivant donne les valeurs possibles de la variable GAIN et l'effet qu'elles ont sur le signal envoyé:

![tblgain](/420-314/images/tblgain.png?width=400px)

Ceci signifie que:
+ Un gain de 1 permet de lire les voltages allant de -4.096V à 4.096V et donc, que la valeur 16bit de -32768 correspond à -4.096V et 32767 correspond à 4.096V.
+ Un gain de 2 permet de lire les voltages allant de -2.048V à 2.048V et donc, que la valeur 16bit de -32768 correspond à -2.048V et 32767 correspond à 2.048V.
+ etc.

En contrepartie, lorsque le gain augmente, il devient possible de détecter des plus petites variations de voltage car on a toujours 16bits pour représenter un ensemble de valeurs moins large. Par exemple, encore selon la table ci-haut:
+ Un gain de 1 permet de lire des variations de voltage de 0.1250mV.
+ Un gain de 2 permet de lire des variations de voltage de 0.0625mV.
+ etc.

> Lorsque le gain augmente, la précision des valeurs lues augmente, mais la plage des valeurs possibles diminue.

{{% notice info "Quelques exercices de compréhension" %}}
Attention: il n'y a pas de réponses précises à ces questions. Comme Le signal qu'on traite est analogique, il peut contenir du bruit, ce qui a pour effet de faire varier plus ou moins fortement les données lues. Rechercez donc des réponses approximatives.
1. Avec un gain de 1, quelle est la valeur maximale lue?
2. Avec un gain de 2, quelle est la valeur maximale lue?
3. Avec un gain de 4, 8 ou 16, à quel moment dans la rotation du bouton atteint-on la valeur maximale lue?

Maintenant branchez votre potentiomètre sur le courant 3.3V.
1. Avec un gain de 1, quelle est la valeur maximum lue?
2. Avec un gain de 2, quelle est la valeur maximum lue?
3. En expérimentant avec différents gains et en lisant les valeurs obtenues, quelle valeur de gain vous semble donner les meilleurs résultats (c'est-à-dire, les résultats les plus utilisables) lorsque le potentiomètre est branché sur 3.3V?
4. Afin de diminuer les variations entre chaque lecture, sauriez-vous faire un programme qui calcule la moyenne des 3 derniers résultats lus et affiche cette moyenne?
   
{{% /notice %}}

<!--
{{% expand "Réponses" %}}
5V:
1. 30000
2. 32767
3. Avant la fin

3.3V:
1. 20
2. 55
{{% /expand %}}
-->



